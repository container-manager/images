variables:
  LC_ALL: "C.UTF-8"
  LANG: "C.UTF-8"

stages:
  - docker
  - test
  - build
  - deploy

docker:
  image: docker:20.10.22
  stage: docker
  services:
    - docker:20.10.22-dind
  variables:
    # Use TLS https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: "/certs"
  script:
    - cd docker/build-img
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY/container-manager/images/build-img:latest .
    - docker push $CI_REGISTRY/container-manager/images/build-img:latest
  rules:
    - changes:
      - docker/build-img/**

lint:
  stage: test
  image: sdesbure/yamllint
  script:
    - yamllint .

build-16.04:
  stage: build
  image: $CI_REGISTRY/container-manager/images/build-img:latest
  when: manual
  script:
    - cd images/ubuntu/16.04/amd64/default
    - nowDir=$(date +%Y%m%d_%H:%M)
    - mkdir -p $nowDir
    - cd $nowDir
    - distrobuilder build-lxd ../distro.yaml
  artifacts:
    paths:
      - images/ubuntu/16.04/amd64/default
    expire_in: 1w
  rules:
    - changes:
      - images/ubuntu/16.04/amd64/default/distro.yaml

build-18.04:
  stage: build
  image: $CI_REGISTRY/container-manager/images/build-img:latest
  when: manual
  script:
    - cd images/ubuntu/18.04/amd64/default
    - nowDir=$(date +%Y%m%d_%H:%M)
    - mkdir -p $nowDir
    - cd $nowDir
    - distrobuilder build-lxd ../distro.yaml
  artifacts:
    paths:
      - images/ubuntu/18.04/amd64/default
    expire_in: 1w
  rules:
    - changes:
      - images/ubuntu/18.04/amd64/default/distro.yaml

build-18.04-docker:
  stage: build
  image: $CI_REGISTRY/container-manager/images/build-img:latest
  when: manual
  script:
    - cd images/ubuntu/18.04/amd64/docker
    - nowDir=$(date +%Y%m%d_%H:%M)
    - mkdir -p $nowDir
    - cd $nowDir
    - distrobuilder build-lxd ../distro.yaml
  artifacts:
    paths:
      - images/ubuntu/18.04/amd64/docker
    expire_in: 1w
  rules:
    - changes:
      - images/ubuntu/18.04/amd64/docker/distro.yaml

build-18.04-conda:
  stage: build
  image: $CI_REGISTRY/container-manager/images/build-img:latest
  when: manual
  script:
    - cd images/ubuntu/18.04/amd64/conda
    - nowDir=$(date +%Y%m%d_%H:%M)
    - mkdir -p $nowDir
    - cd $nowDir
    - distrobuilder build-lxd ../distro.yaml
  artifacts:
    paths:
      - images/ubuntu/18.04/amd64/conda
    expire_in: 1w
  rules:
    - changes:
      - images/ubuntu/18.04/amd64/conda/distro.yaml

build-18.04-condadocker:
  stage: build
  image: $CI_REGISTRY/container-manager/images/build-img:latest
  when: manual
  script:
    - cd images/ubuntu/18.04/amd64/condadocker
    - nowDir=$(date +%Y%m%d_%H:%M)
    - mkdir -p $nowDir
    - cd $nowDir
    - distrobuilder build-lxd ../distro.yaml
  artifacts:
    paths:
      - images/ubuntu/18.04/amd64/condadocker
    expire_in: 1w
  rules:
    - changes:
      - images/ubuntu/18.04/amd64/condadocker/distro.yaml

build-20.04:
  stage: build
  image: $CI_REGISTRY/container-manager/images/build-img:latest
  when: manual
  script:
    - cd images/ubuntu/20.04/amd64/default
    - nowDir=$(date +%Y%m%d_%H:%M)
    - mkdir -p $nowDir
    - cd $nowDir
    - distrobuilder build-lxd ../distro.yaml
  artifacts:
    paths:
      - images/ubuntu/20.04/amd64/default
    expire_in: 1w
  rules:
    - changes:
      - images/ubuntu/20.04/amd64/default/distro.yaml

build-20.04-docker:
  stage: build
  image: $CI_REGISTRY/container-manager/images/build-img:latest
  when: manual
  script:
    - cd images/ubuntu/20.04/amd64/docker
    - nowDir=$(date +%Y%m%d_%H:%M)
    - mkdir -p $nowDir
    - cd $nowDir
    - distrobuilder build-lxd ../distro.yaml
  artifacts:
    paths:
      - images/ubuntu/20.04/amd64/docker
    expire_in: 1w
  rules:
    - changes:
      - images/ubuntu/20.04/amd64/docker/distro.yaml

build-20.04-conda:
  stage: build
  image: $CI_REGISTRY/container-manager/images/build-img:latest
  when: manual
  script:
    - cd images/ubuntu/20.04/amd64/conda
    - nowDir=$(date +%Y%m%d_%H:%M)
    - mkdir -p $nowDir
    - cd $nowDir
    - distrobuilder build-lxd ../distro.yaml
  artifacts:
    paths:
      - images/ubuntu/20.04/amd64/conda
    expire_in: 1w
  rules:
    - changes:
      - images/ubuntu/20.04/amd64/conda/distro.yaml

build-20.04-condadocker:
  stage: build
  image: $CI_REGISTRY/container-manager/images/build-img:latest
  when: manual
  script:
    - cd images/ubuntu/20.04/amd64/condadocker
    - nowDir=$(date +%Y%m%d_%H:%M)
    - mkdir -p $nowDir
    - cd $nowDir
    - distrobuilder build-lxd ../distro.yaml
  artifacts:
    paths:
      - images/ubuntu/20.04/amd64/condadocker
    expire_in: 1w
  rules:
    - changes:
      - images/ubuntu/20.04/amd64/condadocker/distro.yaml

build-22.04:
  stage: build
  image: $CI_REGISTRY/container-manager/images/build-img:latest
  when: manual
  script:
    - cd images/ubuntu/22.04/amd64/default
    - nowDir=$(date +%Y%m%d_%H:%M)
    - mkdir -p $nowDir
    - cd $nowDir
    - distrobuilder build-lxd ../distro.yaml
  artifacts:
    paths:
      - images/ubuntu/22.04/amd64/default
    expire_in: 1w
  rules:
    - changes:
      - images/ubuntu/22.04/amd64/default/distro.yaml

build-22.04-docker:
  stage: build
  image: $CI_REGISTRY/container-manager/images/build-img:latest
  when: manual
  script:
    - cd images/ubuntu/22.04/amd64/docker
    - nowDir=$(date +%Y%m%d_%H:%M)
    - mkdir -p $nowDir
    - cd $nowDir
    - distrobuilder build-lxd ../distro.yaml
  artifacts:
    paths:
      - images/ubuntu/22.04/amd64/docker
    expire_in: 1w
  rules:
    - changes:
      - images/ubuntu/22.04/amd64/docker/distro.yaml

build-22.04-conda:
  stage: build
  image: $CI_REGISTRY/container-manager/images/build-img:latest
  when: manual
  script:
    - cd images/ubuntu/22.04/amd64/conda
    - nowDir=$(date +%Y%m%d_%H:%M)
    - mkdir -p $nowDir
    - cd $nowDir
    - distrobuilder build-lxd ../distro.yaml
  artifacts:
    paths:
      - images/ubuntu/22.04/amd64/conda
    expire_in: 1w
  rules:
    - changes:
      - images/ubuntu/22.04/amd64/conda/distro.yaml

build-22.04-condadocker:
  stage: build
  image: $CI_REGISTRY/container-manager/images/build-img:latest
  when: manual
  script:
    - cd images/ubuntu/22.04/amd64/condadocker
    - nowDir=$(date +%Y%m%d_%H:%M)
    - mkdir -p $nowDir
    - cd $nowDir
    - distrobuilder build-lxd ../distro.yaml
  artifacts:
    paths:
      - images/ubuntu/22.04/amd64/condadocker
    expire_in: 1w
  rules:
    - changes:
      - images/ubuntu/22.04/amd64/condadocker/distro.yaml

build-23.10:
  stage: build
  image: $CI_REGISTRY/container-manager/images/build-img:latest
  when: manual
  script:
    - cd images/ubuntu/23.10/amd64/default
    - nowDir=$(date +%Y%m%d_%H:%M)
    - mkdir -p $nowDir
    - cd $nowDir
    - distrobuilder build-lxd ../distro.yaml
  artifacts:
    paths:
      - images/ubuntu/23.10/amd64/default
    expire_in: 1w
  rules:
    - changes:
      - images/ubuntu/23.10/amd64/default/distro.yaml

build-23.10-docker:
  stage: build
  image: $CI_REGISTRY/container-manager/images/build-img:latest
  when: manual
  script:
    - cd images/ubuntu/23.10/amd64/docker
    - nowDir=$(date +%Y%m%d_%H:%M)
    - mkdir -p $nowDir
    - cd $nowDir
    - distrobuilder build-lxd ../distro.yaml
  artifacts:
    paths:
      - images/ubuntu/23.10/amd64/docker
    expire_in: 1w
  rules:
    - changes:
      - images/ubuntu/23.10/amd64/docker/distro.yaml

build-23.10-conda:
  stage: build
  image: $CI_REGISTRY/container-manager/images/build-img:latest
  when: manual
  script:
    - cd images/ubuntu/23.10/amd64/conda
    - nowDir=$(date +%Y%m%d_%H:%M)
    - mkdir -p $nowDir
    - cd $nowDir
    - distrobuilder build-lxd ../distro.yaml
  artifacts:
    paths:
      - images/ubuntu/23.10/amd64/conda
    expire_in: 1w
  rules:
    - changes:
      - images/ubuntu/23.10/amd64/conda/distro.yaml

build-23.10-condadocker:
  stage: build
  image: $CI_REGISTRY/container-manager/images/build-img:latest
  when: manual
  script:
    - cd images/ubuntu/23.10/amd64/condadocker
    - nowDir=$(date +%Y%m%d_%H:%M)
    - mkdir -p $nowDir
    - cd $nowDir
    - distrobuilder build-lxd ../distro.yaml
  artifacts:
    paths:
      - images/ubuntu/23.10/amd64/condadocker
    expire_in: 1w
  rules:
    - changes:
      - images/ubuntu/23.10/amd64/condadocker/distro.yaml

pages:
  stage: deploy
  image: $CI_REGISTRY/container-manager/images/build-img:latest
  when: manual
  before_script:
    - git clone https://github.com/gartnera/lxd-image-server
    - cd lxd-image-server
    - python3 setup.py install
    - cd -
    - mkdir -p streams/v1
  script:
    - lxd-image-server update --img_dir images --streams_dir streams/v1
    - mkdir public
    - mv images public/
    - mv streams public/
  artifacts:
    paths:
      - public
    expire_in: 3w
