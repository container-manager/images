image:
  distribution: "ubuntu"
  release: bionic

source:
  downloader: debootstrap
  same_as: gutsy
  url: http://archive.ubuntu.com/ubuntu
  keyserver: hkps://keyserver.ubuntu.com
  keys:
    - 0x790BC7277767219C42C86F933B4FE6ACC0B21F32
    - 0xf6ecb3762474eda9d21b7022871920d1991bc93c

targets:
  lxc:
    create_message: |-
      You just created an {{ image.description }} container.

      To enable SSH, run: apt install openssh-server
      No default root or user password are set by LXC.

    config:
      - type: all
        before: 5
        content: |-
          lxc.include = LXC_TEMPLATE_CONFIG/ubuntu.common.conf

      - type: user
        before: 5
        content: |-
          lxc.include = LXC_TEMPLATE_CONFIG/ubuntu.userns.conf

      - type: all
        after: 4
        content: |-
          lxc.include = LXC_TEMPLATE_CONFIG/common.conf

          # For Ubuntu 14.04
          lxc.mount.entry = /sys/kernel/debug sys/kernel/debug none bind,optional 0 0
          lxc.mount.entry = /sys/kernel/security sys/kernel/security none bind,optional 0 0
          lxc.mount.entry = /sys/fs/pstore sys/fs/pstore none bind,optional 0 0
          lxc.mount.entry = mqueue dev/mqueue mqueue rw,relatime,create=dir,optional 0 0

      - type: user
        after: 4
        content: |-
          lxc.include = LXC_TEMPLATE_CONFIG/userns.conf

          # For Ubuntu 14.04
          lxc.mount.entry = /sys/firmware/efi/efivars sys/firmware/efi/efivars none bind,optional 0 0
          lxc.mount.entry = /proc/sys/fs/binfmt_misc proc/sys/fs/binfmt_misc none bind,optional 0 0

      - type: all
        content: |-
          lxc.arch = {{ image.architecture_personality }}

files:
 - path: /etc/hostname
   generator: hostname

 - path: /etc/hosts
   generator: hosts

 - path: /etc/resolvconf/resolv.conf.d/original
   generator: remove

 - path: /etc/resolvconf/resolv.conf.d/tail
   generator: remove

 - path: /etc/machine-id
   generator: remove

 - path: /etc/netplan/10-lxc.yaml
   generator: dump
   content: |-
     network:
       version: 2
       ethernets:
         eth0: {dhcp4: true}
   releases:
     - artful
     - bionic
     - cosmic
     - disco
     - eoan
     - focal
     - groovy
     - hirsute
     - impish
     - jammy

 - path: /etc/network/interfaces
   generator: dump
   content: |-
     # This file describes the network interfaces available on your system
     # and how to activate them. For more information, see interfaces(5).

     # The loopback network interface
     auto lo
     iface lo inet loopback

     auto eth0
     iface eth0 inet dhcp
   releases:
     - trusty
     - xenial

 - path: /etc/init/lxc-tty.conf
   generator: upstart-tty
   releases:
    - trusty

 - name: authorized_keys
   path: /etc/ssh/authorized_keys
   generator: template
   template:
     when:
       - start
   content: |-
     {{ config_get("user.authorized_keys", "") }}

 - name: sshd_config
   path: /etc/ssh/sshd_config
   generator: dump
   template:
     when:
       - start
   content: |-
     ChallengeResponseAuthentication no
     UsePAM yes
     X11Forwarding yes
     PrintMotd no
     AcceptEnv LANG LC_*
     Subsystem  sftp  /usr/lib/openssh/sftp-server
     AuthorizedKeysFile /etc/ssh/authorized_keys
     PasswordAuthentication no

 - name: hostname
   path: /etc/hostname
   generator: template
   template:
     when:
       - start
   content: |-
     {{ config_get("user.remotename", container.name) }}

 - name: hosts
   path: /etc/hosts
   generator: template
   template:
     when:
       - create
       - copy
   content: |-
     127.0.0.1 {{ config_get("user.remotename", container.name) }}

 - name: sudo
   path: /etc/sudoers
   generator: template
   content: |-
     Defaults env_reset
     Defaults secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin"
     root   ALL=(ALL:ALL) ALL
     %sudo  ALL=(ALL:ALL) NOPASSWD:ALL

 - name: configure.sh
   path: /opt/configure.sh
   generator: template
   content: |-
     #!/bin/bash
     groupadd docker
     useradd -ms /bin/bash -G sudo,docker {{ config_get("user.username", "ubuntu") }}
     run-parts /opt/deploy/scripts/

 - name: rc.local
   path: /etc/rc.local
   generator: template
   content: |-
     #!/bin/bash
     useradd -ms /bin/bash {{ config_get("user.username", "ubuntu") }}
     usermod -aG sudo {{ config_get("user.username", "ubuntu") }}
     run-parts /opt/deploy/scripts/

 - path: /etc/systemd/resolved.conf
   generator: dump
   content: |-
    [Resolve]
    MulticastDNS=yes
   releases:
     - artful
     - bionic
     - cosmic
     - disco
     - eoan
     - focal
     - groovy
     - hirsute
     - impish
     - jammy

 - path: /etc/systemd/network/eth0.network
   generator: dump
   content: |-
    [Match]
    Name=eth0

    [Network]
    DHCP=ipv4
    LinkLocalAddressing=ipv6

    [DHCP]
    RouteMetric=100
    UseMTU=true
   releases:
     - artful
     - bionic
     - cosmic
     - disco
     - eoan
     - focal
     - groovy
     - hirsute
     - impish
     - jammy

 - path: /etc/systemd/network/eth1.network
   generator: dump
   content: |-
    [Match]
    Name=eth1

    [Network]
    DHCP=ipv4
    LinkLocalAddressing=ipv6
    MulticastDNS=true

    [DHCP]
    RouteMetric=100
    UseMTU=true
   releases:
     - artful
     - bionic
     - cosmic
     - disco
     - eoan
     - focal
     - groovy
     - hirsute
     - impish
     - jammy

 - path: /etc/systemd/network/eth2.network
   generator: dump
   content: |-
    [Match]
    Name=eth2

    [Network]
    DHCP=ipv4
    LinkLocalAddressing=ipv6
    MulticastDNS=true

    [DHCP]
    RouteMetric=100
    UseMTU=true
   releases:
     - artful
     - bionic
     - cosmic
     - disco
     - eoan
     - focal
     - groovy
     - hirsute
     - impish
     - jammy

 - path: /etc/systemd/network/eth3.network
   generator: dump
   content: |-
    [Match]
    Name=eth3

    [Network]
    DHCP=ipv4
    LinkLocalAddressing=ipv6
    MulticastDNS=true

    [DHCP]
    RouteMetric=100
    UseMTU=true
   releases:
     - artful
     - bionic
     - cosmic
     - disco
     - eoan
     - focal
     - groovy
     - hirsute
     - impish
     - jammy

 - path: /etc/systemd/network/eth4.network
   generator: dump
   content: |-
    [Match]
    Name=eth4

    [Network]
    DHCP=ipv4
    LinkLocalAddressing=ipv6
    MulticastDNS=true

    [DHCP]
    RouteMetric=100
    UseMTU=true
   releases:
     - artful
     - bionic
     - cosmic
     - disco
     - eoan
     - focal
     - groovy
     - hirsute
     - impish
     - jammy

 - path: /etc/systemd/network/eth5.network
   generator: dump
   content: |-
    [Match]
    Name=eth5

    [Network]
    DHCP=ipv4
    LinkLocalAddressing=ipv6
    MulticastDNS=true

    [DHCP]
    RouteMetric=100
    UseMTU=true
   releases:
     - artful
     - bionic
     - cosmic
     - disco
     - eoan
     - focal
     - groovy
     - hirsute
     - impish
     - jammy

packages:
  manager: apt
  update: true
  cleanup: true

  sets:
    - packages:
      - apt-transport-https
      - language-pack-en
      - openssh-client
      - openssh-server
      - ssh
      - policykit-1
      - vim
      - curl
      - git
      - tmux
      action: install

  repositories:
    - name: sources.list
      url: |-
        deb http://archive.ubuntu.com/ubuntu {{ image.release }} main restricted universe multiverse
        deb http://archive.ubuntu.com/ubuntu {{ image.release }}-updates main restricted universe multiverse
        deb http://security.ubuntu.com/ubuntu {{ image.release }}-security main restricted universe multiverse
      architectures:
        - amd64
        - i386

    - name: sources.list
      url: |-
        deb http://ports.ubuntu.com/ubuntu-ports {{ image.release }} main restricted universe multiverse
        deb http://ports.ubuntu.com/ubuntu-ports {{ image.release }}-updates main restricted universe multiverse
        deb http://ports.ubuntu.com/ubuntu-ports {{ image.release }}-security main restricted universe multiverse
      architectures:
      - armhf
      - arm64
      - powerpc
      - powerpc64
      - ppc64el
      - riscv64

actions:
  - trigger: post-update
    action: |-
      #!/bin/sh
      set -eux

  - trigger: post-packages
    action: |-
      #!/bin/sh
      set -eux

      # Make sure the locale is built and functional
      locale-gen en_US.UTF-8
      update-locale LANG=en_US.UTF-8

      # Inject the configure service.
      echo "[Unit]
      Description=container-manager configuration

      [Service]
      Type=oneshot
      ExecStart=/bin/bash /opt/configure.sh

      [Install]
      WantedBy=network-online.target" > /etc/systemd/system/configure.service

      # Enable the configure service
      targetDir=/etc/systemd/system/default.target.wants/
      mkdir -p $targetDir
      ln -s /etc/systemd/system/configure.service $targetDir/configure.service

      apt purge -y netplan.io
      apt install -y --no-install-recommends docker.io pigz
      wget -O /usr/local/bin/docker-compose https://github.com/docker/compose/releases/download/1.24.1/docker-compose-Linux-x86_64
      sudo chmod +x /usr/local/bin/docker-compose
      apt autoremove -y
      apt clean
      rm -rf /var/lib/apt/lists/*

      # Cleanup underlying /run
      mount -o bind / /mnt
      rm -rf /mnt/run/*
      umount /mnt

      rm -rf /var/lib/apt/lists

      # Cleanup temporary shadow paths
      rm /etc/*-
      touch /etc/rc.local
      chmod +x /etc/rc.local

mappings:
  architecture_map: debian
